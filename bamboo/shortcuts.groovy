checkoutPLUGIN(['repoName']) {
    task(type:'checkout', description:'Checkout Plugin'){
        repository(name:'#repoName', checkoutDirectory: 'PLUGIN')
    }
}

checkoutJIRA(['repoName']) {
    task(type:'checkout', description:'Checkout JIRA'){
        repository(name:'#repoName', checkoutDirectory: 'JIRA')
    }
}

generateJIRAandPLUGINpoms() {
    task(description:'Generate poms to build JIRA and PLUGIN together',
            type:'script',
            script:'JIRA/bin/generate-maven-aggregator-pom.sh',
            argument: 'JIRA PLUGIN'
    )
}

compileJIRA(['jdkVersion']) {
    task(description:'Build and install artifacts required by AMPS',
            type:'maven3',
            goal:'clean install -B -am -Pdistribution -pl JIRA -Dmaven.test.skip -DskipSources',
            buildJdk:'JDK #jdkVersion',
            mavenExecutable:'Maven 3.0',
            environmentVariables:'MAVEN_OPTS="${bamboo.maven.opts}"',
    )
}

compileJIRAandPLUGIN(['jdkVersion']) {
    task(description:'Build and install artifacts required by PLUGIN unit tests',
            type:'maven3',
            goal:'clean install -B -am -DskipTests',
            buildJdk:'JDK #jdkVersion',
            mavenExecutable:'Maven 3.0',
            environmentVariables:'MAVEN_OPTS="${bamboo.maven.opts}"'
    )
}

runUnitTests(['jdkVersion']) {
    task(description:'Run the unit tests with the wallboards category',
            type:'maven3',
            goal:'-B test',
            buildJdk:'JDK #jdkVersion',
            mavenExecutable:'Maven 3.0',
            environmentVariables:'MAVEN_OPTS="${bamboo.maven.opts}"',
            workingSubDirectory:'PLUGIN',
            hasTests:'true'
    )
}

clean(['jdkVersion']) {
    task(description:'Remove installed artifacts',
            final:'true',
            type:'maven3',
            goal:'build-helper:remove-project-artifact -Dbuildhelper.removeAll=false -Pdistribution -pl JIRA -am',
            buildJdk:'JDK #jdkVersion',
            mavenExecutable:'Maven 3.0',
            environmentVariables:'MAVEN_OPTS="${bamboo.maven.opts}"'
    )
}

requireJDK(['jdkVersion']) {
    requireLinux()
    requirement(key:'system.jdk.JDK #jdkVersion', condition:'exists')
}

requireNode() {
    requirement(key:'os',                          condition:'equals', value:'Linux')
    requirement(key:'system.builder.node.Node.js', condition:'exists')
}

requireLinux() {
    requirement(key:'os',             condition:'equals', value:'Linux')
}

repositoryPLUGIN(['repoName']) {
    repository(name:'#repoName')
    trigger(description:'Plugin every 1 minute', type:'polling', strategy:'periodically', frequency:'60'){
        repository(name:'#repoName')
    }
}

repositoryJIRA(['repoName']) {
    repository(name:'#repoName')
    trigger(description:'JIRA every 6 hours', type:'polling', strategy:'periodically', frequency:'21600'){
        repository(name:'#repoName')
    }
}

wallboardsLabels() {
    label(name:'wallboards')
    label(name:'plan-templates')
}

wallboardsNotifications() {
    notification(type: 'Comment Added', recipient: 'responsible')
    notification(type: 'Job Hung', recipient: 'responsible')
    notification(type: 'Failed Jobs and First Successful', recipient: 'responsible')

    notification(type: 'Comment Added', recipient: 'watchers')
    notification(type: 'Job Hung', recipient: 'watchers')
    notification(type: 'Failed Jobs and First Successful', recipient: 'watchers')
}

wallboardsProject() {
    project(key: 'WALL', name:'Wallboards Plugin', description:'Wallboards Plugin')
}

branches(['pattern']) {
    branchMonitoring(enabled: 'true',
            matchingPattern: '#pattern',
            timeOfInactivityInDays: '7',
            notificationStrategy: 'INHERIT',
            remoteJiraBranchLinkingEnabled: 'true'
    )
}

jUnitTests(['pluginRepo','jiraRepo', 'jdkVersion']) {
    job(name: 'JUnit Tests', key: 'JUNIT', description: 'JUnit Tests' ){
        requireJDK(jdkVersion: '#jdkVersion')

        checkoutPLUGIN(repoName:'#pluginRepo')
        checkoutJIRA(repoName:'#jiraRepo')
        generateJIRAandPLUGINpoms()
        compileJIRAandPLUGIN(jdkVersion: '#jdkVersion')
        runUnitTests(jdkVersion: '#jdkVersion')
        clean(jdkVersion: '#jdkVersion')
    }
}

functionalTests(['pluginRepo','jiraRepo', 'jdkVersion']) {
    job(name: 'Functional Tests', key: 'FUNC', description: 'Functional Tests' ){
        requireJDK(jdkVersion: '#jdkVersion')

        artifactDefinition(name:'Heap dumps', pattern:'**/*.hprof')
        artifactDefinition(name:'jira logs', pattern:'**/*.log')
        artifactDefinition(name:'Surefire reports', location:'PLUGIN/target/surefire-reports', pattern:'*')
        artifactDefinition(name:'Test reports', location:'PLUGIN/target/test-reports', pattern:'*')

        checkoutPLUGIN(repoName:'#pluginRepo')
        checkoutJIRA(repoName:'#jiraRepo')
        generateJIRAandPLUGINpoms()
        compileJIRA(jdkVersion: '#jdkVersion')
        runFuncTests(jdkVersion: '#jdkVersion')
        clean(jdkVersion: '#jdkVersion')
    }
}

releaseTask(['pluginRepo', 'jiraRepo', 'jdkVersion']) {
    job(name:"Release", key:'RELEASE', description:'Release'){
        requireJDK(jdkVersion: '#jdkVersion')
        checkoutPLUGIN(repoName:'#pluginRepo')
        task(description:'Run release.sh',
                type:'script',
                script:'release.sh',
                environmentVariables:'MAVEN_OPTS="${bamboo.maven.opts}" M2_HOME=${bamboo.capability.system.builder.mvn3.Maven 3.0}',
                workingSubDirectory: 'PLUGIN'
        )
    }
}

runFuncTests(['jdkVersion']) {
    task(description:'Run the func tests',
            type:'script',
            script:'run.sh',
            argument: '-B clean verify -Dselenium.browser.path=${bamboo.capability.firefox latest}/firefox -Djava.awt.headless=true -Dmaven.test.failure.ignore=true -PrunIndividualTestSuite -DtestGroups=run#typeTest -DnoDevMode',
            environmentVariables:'MAVEN_OPTS="${bamboo.maven.opts}" M2_HOME="${bamboo.capability.system.builder.mvn3.Maven 3.0}" JAVA_HOME="${bamboo.capability.system.jdk.JDK #jdkVersion}"',
            workingSubDirectory:'PLUGIN'
    )
    task(description:'Parse JUnit results',
            final:'true',
            type:'jUnitParser',
            resultsDirectory:'PLUGIN/target/group-__no_test_group__/tomcat6x/surefire-reports/*.xml'
    )
}
