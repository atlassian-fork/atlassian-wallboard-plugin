package it.com.atlassian.jirawallboard;

import com.atlassian.jira.pageobjects.config.LoginAs;
import com.atlassian.jira.pageobjects.pages.ConfigureWallboardPage;
import com.atlassian.jira.pageobjects.pages.DashboardPage;
import com.atlassian.pageobjects.elements.query.Poller;
import org.junit.Test;

/**
 * This is a simple test to verify that the wallboards plugin is present and not catastrophically broken.
 */
public class WallboardsIntegrationSmokeTest extends AbstractWallboardTest
{
    /**
     * Check that the wallboard plugin successfully loads and the wallboard options are
     * accessible from the Tools dropdown on the home page when viewing the system dashboard
     */
    @Test
    @LoginAs(sysadmin = true, targetPage = DashboardPage.class)
    public void testWallboardOptionAndPageInJIRA()
    {
        DashboardPage dashboardPage = pageBinder.bind(DashboardPage.class);
        ConfigureWallboardPage configureWallboardPage = dashboardPage.configureWallboard();
        Poller.waitUntilTrue(configureWallboardPage.isVisible());
        dashboardPage = configureWallboardPage.cancel();
        dashboardPage.viewAsWallboard();
        Poller.waitUntilTrue(dashboardPage.isViewingWallboard());
    }
}
