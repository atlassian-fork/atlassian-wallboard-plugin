package com.atlassian.jirawallboard.servlet;

import junit.framework.TestCase;

public class RequestParameterSanitizerTest extends TestCase {

    public void testMaliciousCyclePeriod() {
        String maliciousCyclePeriod = "(function()\n" +
                "{alert(document.cookie);return 30000;}\n" +
                "\n" +
                ")()";
        int sanitizationResult = RequestParameterSanitizer.sanitizeOrDefaultCyclePeriod(maliciousCyclePeriod);
        assertEquals(0, sanitizationResult);
    }

    public void testNegativeCyclePeriod() {
        int sanitizationResult = RequestParameterSanitizer.sanitizeOrDefaultCyclePeriod("-123");
        assertEquals(0, sanitizationResult);
    }

    public void testZeroCyclePeriod() {
        int sanitizationResult = RequestParameterSanitizer.sanitizeOrDefaultCyclePeriod("0");
        assertEquals(0, sanitizationResult);
    }

    public void testValidCyclePeriod() {
        int sanitizationResult = RequestParameterSanitizer.sanitizeOrDefaultCyclePeriod("30000");
        assertEquals(30000, sanitizationResult);
    }

    public void testValidTransitionFx() {
        String fadeZoom = "fadeZoom";
        String sanitizationResult = RequestParameterSanitizer.sanitizeTransitionFx(fadeZoom);
        assertEquals("fadeZoom", sanitizationResult);
    }
}
