package com.atlassian.jirawallboard.servlet;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jirawallboard.MockPluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class WallboardTest  {

    @Mock
    private BuildUtilsInfo buildUtilsInfo;

    @Mock
    private PluginSettingsFactory psf;

    @Mock
    private UserManager userManager;

    private MockApplicationUser otherUser;

    @Before
    public void setUp() throws Exception {
        MockPluginSettings ps = new MockPluginSettings();

        //stuff for UserCompatibility lib
        MockComponentWorker mockComponentWorker = new MockComponentWorker();
        ComponentAccessor.initialiseWorker(mockComponentWorker);
        mockComponentWorker.addMock(BuildUtilsInfo.class, buildUtilsInfo);
        mockComponentWorker.addMock(UserManager.class, userManager);
        int[] version = {5, 0 ,0};
        when(buildUtilsInfo.getVersionNumbers()).thenReturn(version);


        when(psf.createGlobalSettings()).thenReturn(ps);

        MockApplicationUser user = new MockApplicationUser("user");
        when(userManager.getUserByName("user")).thenReturn(user);
        otherUser = new MockApplicationUser("otherUser");
        when(userManager.getUserByName("otherUser")).thenReturn(otherUser);

        WallboardPluginSettings wallboardPluginSettings = new WallboardPluginSettings(psf, user);

        List<String> dashboardIds = new LinkedList<>();
        dashboardIds.add("12345");
        dashboardIds.add("12346");
        dashboardIds.add("12347");
        dashboardIds.add("12348");

        wallboardPluginSettings.setDashboardIds(dashboardIds);
        wallboardPluginSettings.saveChanges();
    }

    @Test
    public void testConfiguredWallboardServletContextProvider() throws Exception {
        ConfiguredWallboardServletContextProvider contextProvider = new ConfiguredWallboardServletContextProvider(psf);
        Map<String, Object> map = new HashMap<>();
        map.put("username", "user");
        Map<String, Object> contextMap = contextProvider.getContextMap(map);
        assertTrue(((String) contextMap.get("wallboardString")).contains
                ("?dashboardId=12345&dashboardId=12346&dashboardId=12347&dashboardId=12348"));
    }

    @Test
    public void testWalboardPluginSettings() throws Exception {
        WallboardPluginSettings wallboardPluginSettings = WallboardPluginSettings.loadSettings(psf, "user");
        assertTrue(wallboardPluginSettings.isConfigured());
        List<String> savedIds = wallboardPluginSettings.getDashboardIds();
        assertTrue(savedIds.get(0).equals("12345"));
        assertTrue(savedIds.get(1).equals("12346"));
        assertTrue(savedIds.get(2).equals("12347"));
        assertTrue(savedIds.get(3).equals("12348"));
        assertTrue((new WallboardPluginSettings(psf, otherUser)).getDashboardIds().size() == 0);

    }
}
