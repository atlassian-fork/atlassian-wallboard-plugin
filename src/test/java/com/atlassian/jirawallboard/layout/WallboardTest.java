package com.atlassian.jirawallboard.layout;

import com.atlassian.gadgets.GadgetRequestContext;
import com.atlassian.gadgets.GadgetState;
import com.atlassian.gadgets.dashboard.Color;
import com.atlassian.gadgets.dashboard.Layout;
import com.atlassian.gadgets.view.RenderedGadgetUriBuilder;
import com.atlassian.gadgets.view.View;
import junit.framework.TestCase;
import org.mockito.Matchers;

import java.net.URI;
import java.util.Iterator;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class WallboardTest extends TestCase {

    public void testWallboardLayouts() throws Exception {
        RenderedGadgetUriBuilder renderedGadgetUriBuilder = mock(RenderedGadgetUriBuilder.class);
        WallboardLayout oneCol = new WallboardLayout(null, null, renderedGadgetUriBuilder, Layout.A);
        WallboardLayout twoCol = new WallboardLayout(null, null, renderedGadgetUriBuilder, Layout.AA);
        WallboardLayout threeCol = new WallboardLayout(null, null, renderedGadgetUriBuilder, Layout.AAA);

        URI uri = new URI("");
        when(renderedGadgetUriBuilder.build(Matchers.<GadgetState>any(), Matchers.<View>any(), Matchers.<GadgetRequestContext>any())).thenReturn(uri);

        boolean noException = true;
        try {
            oneCol.addGadget(1, null, null);
        } catch (WallboardLayoutException e) {
            noException = false;
        }
        assert(!noException);

        noException = true;
        try {
            oneCol.addGadget(2, null, null);
        } catch (WallboardLayoutException e) {
            noException = false;
        }
        assert(!noException);

        noException = true;
        try {
            twoCol.addGadget(2, null, null);
        } catch (WallboardLayoutException e) {
            noException = false;
        }
        assert(!noException);

        noException = true;
        try {
            threeCol.addGadget(3, null, null);
        } catch (WallboardLayoutException e) {
            noException = false;
        }
        assert(!noException);

        noException = true;
        try {
            twoCol.addGadget(-1, null, null);
        } catch (WallboardLayoutException e) {
            noException = false;
        }
        assert(!noException);
    }

    public void testWallboardColumn() throws Exception {
        WallboardColumn column = new WallboardColumn();

        String gadget1 = "url1";
        String gadget2 = "url2";
        String gadget3 = "url3";
        String gadget4 = "url4";

        column.addGadget(gadget1, Color.color1, true, null, 100);
        column.addGadget(gadget2, Color.color2, true, null, 100);
        column.addGadget(gadget3, Color.color2, true, null, 100);
        column.addGadget(gadget4, Color.color4, true, null, 100);

        List<WallboardColumn.WallboardItemCollection> gadgets = column.getItemSlots();
        assert(gadgets.size() == 3);
        assert(gadgets.get(0).getColour() == Color.color1);
        assert(gadgets.get(1).getColour() == Color.color2);
        assert(gadgets.get(2).getColour() == Color.color4);

        assert(gadgets.get(0).iterator().next().getUrl().equals(gadget1));
        Iterator<WallboardColumn.WallboardItem> it = gadgets.get(1).iterator();
        assert(it.next().getUrl().equals(gadget2));
        assert(it.next().getUrl().equals(gadget3));
        assert(gadgets.get(2).iterator().next().getUrl().equals(gadget4));

    }
    
}
