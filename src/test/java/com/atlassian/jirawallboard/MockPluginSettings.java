package com.atlassian.jirawallboard;

import com.atlassian.sal.api.pluginsettings.PluginSettings;

import java.util.HashMap;

public class MockPluginSettings extends HashMap<String, Object> implements PluginSettings {

    public Object get(String s) {
        return super.get(s);
    }

    public Object remove(String s) {
        return super.remove(s);
    }
}
