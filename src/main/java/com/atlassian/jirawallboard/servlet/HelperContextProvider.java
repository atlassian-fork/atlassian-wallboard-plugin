package com.atlassian.jirawallboard.servlet;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;

import java.util.Map;

public class HelperContextProvider implements ContextProvider
{

    public void init(Map<String, String> stringStringMap) throws PluginParseException
    {
    }

    public Map<String, Object> getContextMap(Map<String, Object> stringObjectMap)
    {
        stringObjectMap.put("hash", "#");
        return stringObjectMap;
    }
}
