package com.atlassian.jirawallboard.servlet;

import io.atlassian.fugue.Option;
import com.atlassian.gadgets.DashboardItemState;
import com.atlassian.gadgets.DashboardItemStateVisitor;
import com.atlassian.gadgets.GadgetParsingException;
import com.atlassian.gadgets.GadgetRequestContext;
import com.atlassian.gadgets.GadgetRequestContextFactory;
import com.atlassian.gadgets.GadgetState;
import com.atlassian.gadgets.LocalDashboardItemState;
import com.atlassian.gadgets.dashboard.DashboardId;
import com.atlassian.gadgets.dashboard.DashboardItemRepresentation;
import com.atlassian.gadgets.dashboard.DashboardItemRepresentationService;
import com.atlassian.gadgets.dashboard.DashboardItemRepresentationService.RenderingContext;
import com.atlassian.gadgets.dashboard.DashboardService;
import com.atlassian.gadgets.dashboard.DashboardState;
import com.atlassian.gadgets.dashboard.DashboardState.ColumnIndex;
import com.atlassian.gadgets.dashboard.PermissionException;
import com.atlassian.gadgets.spec.GadgetSpecFactory;
import com.atlassian.gadgets.view.RenderedGadgetUriBuilder;
import com.atlassian.gadgets.view.View;
import com.atlassian.gadgets.view.ViewType;
import com.atlassian.jira.dashboard.permission.GadgetPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jirawallboard.layout.WallboardLayout;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class WallboardServlet extends HttpServlet
{
    public static class WallboardParam<T>
    {
        private final String key;
        private final T defaultValue;

        private WallboardParam(String key, T defaultValue)
        {
            this.key = key;
            this.defaultValue = defaultValue;
        }

        public String getKey()
        {
            return key;
        }

        public T getDefaultValue()
        {
            return defaultValue;
        }
    }

    public static final WallboardParam<String[]> DASHBOARD_ID = new WallboardParam<String[]>("dashboardId", new String[0]);
    public static final WallboardParam<Integer> CYCLE_PERIOD = new WallboardParam<Integer>("cyclePeriod", 30);
    public static final WallboardParam<String> TRANSITION_FX = new WallboardParam<String>("transitionFx", "none");
    public static final WallboardParam<Boolean> RANDOM = new WallboardParam<Boolean>("random", false);

    private final RenderedGadgetUriBuilder renderedGadgetUriBuilder;
    private final DashboardService dashboardService;
    private final GadgetRequestContextFactory gadgetRequestContextFactory;
    private final GadgetSpecFactory gadgetSpecFactory;
    private final WebResourceManager webResourceManager;
    private final GadgetPermissionManager gadgetPermissionManager;
    private final TemplateRenderer renderer;
    private final JiraAuthenticationContext authContext;
    private final DashboardItemRepresentationService dashboardItemRepresentationService;

    private View wallboardView;

    public static final Logger log = Logger.getLogger(WallboardServlet.class);

    public WallboardServlet(RenderedGadgetUriBuilder renderedGadgetUriBuilder, DashboardService dashboardService, GadgetRequestContextFactory gadgetRequestContextFactory,
            TemplateRenderer renderer, GadgetSpecFactory gadgetSpecFactory, WebResourceManager webResourceManager,
            GadgetPermissionManager gadgetPermissionManager, JiraAuthenticationContext authContext, final DashboardItemRepresentationService dashboardItemRepresentationService)
    {
        this.renderedGadgetUriBuilder = renderedGadgetUriBuilder;
        this.dashboardService = dashboardService;
        this.gadgetRequestContextFactory = gadgetRequestContextFactory;
        this.gadgetSpecFactory = gadgetSpecFactory;
        this.webResourceManager = webResourceManager;
        this.renderer = renderer;
        this.gadgetPermissionManager = gadgetPermissionManager;
        this.authContext = authContext;
        this.dashboardItemRepresentationService = dashboardItemRepresentationService;

        ViewType viewType = null;
        try
        {

            viewType = ViewType.createViewType("WALLBOARD", "wallboard", "Wallboard", "WallBoard");
            //If we get an IAE we know that such a ViewType already exists and we can just retrieve it.
        }
        catch (IllegalArgumentException iae)
        {
            for (String alias : Arrays.asList("WALLBOARD", "wallboard", "Wallboard", "WallBoard"))
            {
                try
                {
                    viewType = ViewType.valueOf(alias);
                }
                catch (IllegalArgumentException e)
                {
                    //Ignore
                }
            }
        }
        if (viewType == null)
        {
            throw new RuntimeException("Could not create or find view type");
        }

        this.wallboardView = (new View.Builder()).viewType(viewType).build();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        webResourceManager.requireResource("com.atlassian.gadgets.dashboard:dashboard");
        webResourceManager.requireResourcesForContext("atl.dashboard");

        //Doesn't need a decorator,intended to not look like a Jira page.
        String[] rawDashboardIds = req.getParameterValues(DASHBOARD_ID.getKey());
        String contextPath = req.getContextPath();
        ApplicationUser user = authContext.getUser();
        log.log(Level.DEBUG, "Session id is: " + req.getSession().getId());
        if (rawDashboardIds == null)
        {
            rawDashboardIds = new String[0];
        }

        doGetMultipleWallboards(req, resp, rawDashboardIds, contextPath, user);
    }

    private void doGetMultipleWallboards(HttpServletRequest req, HttpServletResponse resp,
            String[] rawDashboardIds, String contextPath, ApplicationUser user)
            throws IOException
    {

        req.setAttribute("servletKey", "wallboard");
        resp.setContentType("text/html");
        MultiWallboardParams params = new MultiWallboardParams();
        params.setWallboardMetadata(new WallframeParams.WallboardMetadata(contextPath));

        StringWriter writer = new StringWriter();
        webResourceManager.requireResourcesForContext("wallboard");
        webResourceManager.includeResources(writer, UrlMode.AUTO);
        params.setResourceIncludes(writer.toString());

        for (String rawDashboardId : rawDashboardIds)
        {
            boolean addDashboard = true;
            DashboardId dashboardId = null;
            try
            {
                dashboardId = DashboardId.valueOf(rawDashboardId);
                dashboardService.get(dashboardId, user != null ? user.getName() : null);
            }
            catch (PermissionException e)
            {
                addDashboard = false;
            }
            if (addDashboard && dashboardId != null)
            {

                DashboardState dashboardState;
                try
                {
                    dashboardState = dashboardService.get(DashboardId.valueOf(dashboardId.toString()), user != null ? user.getName() : null);
                }
                catch (PermissionException e)
                {
                    log.log(Level.DEBUG, "Skipping dashboard " + dashboardId + " because of " + e.getCause());
                    continue;
                }
                catch (NumberFormatException e)
                {
                    log.log(Level.DEBUG, "Skipping dashboard " + dashboardId + " because of " + e.getCause());
                    continue;
                }
                if (dashboardState == null)
                {
                    log.log(Level.DEBUG, "Skipping dashboard " + dashboardId + " because dashboardState is null");
                    continue;
                }
                GadgetRequestContext requestContext = gadgetRequestContextFactory.getBuilder(req).view(wallboardView).build();
                WallboardLayout layout = scrapeDashboard(dashboardState, requestContext, user);
                WallframeParams wallframeParams = layout.generateParams(contextPath);
                params.appendWallframe(wallframeParams);
            }
        }

        Map<String, Object> context = new HashMap<String, Object>();
        context.put(WallboardLayout.PARAMS, params);
        context.put(TRANSITION_FX.getKey(), RequestParameterSanitizer.sanitizeTransitionFx(req.getParameter(TRANSITION_FX.getKey())));
        context.put(RANDOM.getKey(), req.getParameter(RANDOM.getKey()));

        int sanitized = RequestParameterSanitizer.sanitizeOrDefaultCyclePeriod(req.getParameter(CYCLE_PERIOD.getKey()));
        if (sanitized != 0) {
            context.put(CYCLE_PERIOD.getKey(), sanitized);
        }

        renderer.render("/templates/multi-wallboard.vm", context, resp.getWriter());
    }

    private WallboardLayout scrapeDashboard(final DashboardState dashboardState, final GadgetRequestContext gadgetRequestContext, final ApplicationUser user)
    {

        final WallboardLayout layout = new WallboardLayout(gadgetRequestContext, wallboardView, renderedGadgetUriBuilder, dashboardState.getLayout());

        // filter out the gadgets we can't see.
        DashboardState dashboardStateWithPermissionsApplied = gadgetPermissionManager.filterGadgets(dashboardState, user);

        int columnIndex = 0;
        for (Iterable<DashboardItemState> column : dashboardStateWithPermissionsApplied.getDashboardColumns().getColumns())
        {
            for (final DashboardItemState dashboardItem : column)
            {
                final int finalColumnIndex = columnIndex;
                dashboardItem.accept(new DashboardItemStateVisitor<Void>()
                {
                    public Void visit(final GadgetState gadget)
                    {
                        try
                        {
                            layout.addGadget(finalColumnIndex, gadget, gadgetSpecFactory.getGadgetSpec(gadget, gadgetRequestContext));
                        }
                        catch (GadgetParsingException e)
                        {
                            log.log(Level.WARN, "Ignoring gadget with id: " + dashboardItem.getId());
                        }
                        return null;
                    }

                    public Void visit(final LocalDashboardItemState localDashboardItemState)
                    {
                        RenderingContext renderingContext = RenderingContext.readOnly(
                                gadgetRequestContext,
                                dashboardState.getId(),
                                ColumnIndex.from(finalColumnIndex));

                        Option<DashboardItemRepresentation> representation = dashboardItemRepresentationService.getRepresentation(dashboardItem, renderingContext);
                        if (representation.isDefined())
                        {
                            layout.addDashboardItem(finalColumnIndex, dashboardItem, representation.get());
                        }
                        return null;
                    }
                });
            }
            columnIndex++;
        }
        return layout;
    }
}
