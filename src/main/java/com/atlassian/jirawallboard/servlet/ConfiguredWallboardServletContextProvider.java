package com.atlassian.jirawallboard.servlet;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

import java.util.Map;

public class ConfiguredWallboardServletContextProvider implements ContextProvider
{
    private final PluginSettingsFactory pluginSettingsFactory;

    public ConfiguredWallboardServletContextProvider(PluginSettingsFactory pluginSettingsFactory)
    {
        this.pluginSettingsFactory = pluginSettingsFactory;
    }

    public void init(Map<String, String> params) throws PluginParseException
    {
    }

    public Map<String, Object> getContextMap(Map<String, Object> context)
    {
        WallboardPluginSettings wallboardPluginSettings = WallboardPluginSettings.loadSettings(pluginSettingsFactory, (String) context.get("username"));
        if (wallboardPluginSettings.isConfigured())
        {
            StringBuilder stringBuilder = new StringBuilder();
            boolean first = true;
            for (String s : wallboardPluginSettings.getDashboardIds())
            {
                if (first)
                {
                    stringBuilder.append("?");
                    first = false;
                }
                else
                {
                    stringBuilder.append("&");
                }
                stringBuilder.append(WallboardServlet.DASHBOARD_ID.getKey() + "=").append(s);
            }
            stringBuilder.append("&");
            stringBuilder.append(WallboardServlet.CYCLE_PERIOD.getKey()).append("=").append(wallboardPluginSettings.getCyclePeriod() * 1000);
            stringBuilder.append("&");
            stringBuilder.append(WallboardServlet.TRANSITION_FX.getKey()).append("=").append(wallboardPluginSettings.getTransitionFx());
            stringBuilder.append("&");
            stringBuilder.append(WallboardServlet.RANDOM.getKey()).append("=").append(wallboardPluginSettings.isRandom());
            context.put("wallboardString", stringBuilder.toString());
        }
        else
        {
            context.put("wallboardString", "");
        }
        return context;
    }
}
