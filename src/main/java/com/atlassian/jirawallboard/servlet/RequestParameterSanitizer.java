package com.atlassian.jirawallboard.servlet;

import com.atlassian.gzipfilter.org.apache.commons.lang.StringEscapeUtils;

public class RequestParameterSanitizer {

    public static String sanitizeTransitionFx(String transitionFx) {
        return StringEscapeUtils.escapeJavaScript(transitionFx);
    }

    public static int sanitizeOrDefaultCyclePeriod(String cyclePeriod) {
        int sanitized;
        try {
            sanitized = Integer.parseInt(cyclePeriod);
        }catch (IllegalArgumentException e){
            return 0;
        }
        return sanitized<=0?0:sanitized;
    }
}
