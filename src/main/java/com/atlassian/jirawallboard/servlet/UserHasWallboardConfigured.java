package com.atlassian.jirawallboard.servlet;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

import java.util.Map;

public class UserHasWallboardConfigured implements Condition
{
    private final PluginSettingsFactory pluginSettingsFactory;

    public UserHasWallboardConfigured(PluginSettingsFactory pluginSettingsFactory)
    {
        this.pluginSettingsFactory = pluginSettingsFactory;
    }

    public void init(Map<String, String> params) throws PluginParseException
    {
    }

    public boolean shouldDisplay(Map<String, Object> context)
    {
        WallboardPluginSettings wallboardPluginSettings = WallboardPluginSettings.loadSettings(pluginSettingsFactory, (String) context.get("username"));
        return wallboardPluginSettings.isConfigured();
    }

}
