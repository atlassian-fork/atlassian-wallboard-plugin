/**
 * Implementation of setTitle for IfrGadgetService.
 */

define('atlassian-wallboard-plugin/renderWallboard',['underscore', 'jquery'], function (_, $) {
    /**
     * This should probably be in AUI? AJS-548
     * @param uri
     * @param strict
     */

    var AJS = window.AJS || {};
    AJS.WALLBOARD = AJS.WALLBOARD || {};
    AJS.parseUri = function (uri, strict) {
        function parseUri(str) {
            var o = parseUri.options,
                    m = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
                    uri = {},
                    i = 14;

            while (i--) {
                uri[o.key[i]] = m[i] || "";
            }

            uri[o.q.name] = {};
            uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
                if ($1) {
                    uri[o.q.name][$1] = $2;
                }
            });

            return uri;
        }

        parseUri.options = {
            strictMode: !!strict,
            key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
            q:   {
                name:   "queryKey",
                parser: /(?:^|&)([^&=]*)=?([^&]*)/g
            },
            parser: {
                strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
                loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
            }
        };

        uri = parseUri(uri);

        uri.toString = function () {
            var params = [];
            $.each(uri.queryKey, function (name, value) {
                params.push(name + "=" + value);
            });
            return uri.protocol + "://" + uri.authority + uri.path + "?" + params.join("&") + "#" + uri.anchor;
        };

        return uri;
    };

    //Generates a marked up table representing wallframe.
    AJS.WALLBOARD.generateWallframe = function(wallframe) {
        var table = $("<table class='wallframe' id='wallframe-" + wallframe.id + "'><tr></tr></table>");
        var row = table.find("tr");
        for (var i = 0, l = wallframe.columns.length; i < l; i++) {
            row.append("<td class='" + wallframe.columns[i] + "' id='wallframe-" + wallframe.id + "-column-" + i + "'>");
        }

        return table;
    };

    AJS.WALLBOARD.iframesLoaded = 0;
    AJS.WALLBOARD.numIframes = undefined;

    //Takes in a laid-out table which is in the dom, and a wallframe json object and populates it.
    AJS.WALLBOARD.populateWallframe = function($wallframe, wallframe) {
        _.each(wallframe.slots, function(slot) {
            var slotId = slot.column + '-' + slot.slotNumber;
            var $slot = $("<div id='" + slotId + "'>").addClass("slot").appendTo("#" + slot.column);
            //If slot is the only slot in its column (and it isn't just a spacer), then find out how much height is
            // available to it (by taking it's parent's available height) and update slot.height accordingly
            if (slot.singleSlot && !slot.spacerOnly) {
                slot.height = $slot.parent().height() - parseInt($slot.css("margin-bottom"));
            }

            //Now set the height to whatever is in slot.height
            $slot.height(slot.height);

            //If the slot is only a spacer then we shouldn't have any padding (to allow a user to specify exactly how
            // much spacer to include).
            if (slot.spacerOnly) {
                $slot.addClass("spacer-only");
            }

            AJS.WALLBOARD.slotHeights[slotId] = slot.height;
            _.each(slot.gadgets, function(gadget) {

                var gadgetHtml = function() {

                    if (gadget.url) {
                        var $gadget = $("<iframe scrolling='no' marginheight='0' marginwidth='0' frameborder='0' class='wallboard-gadget' id='" + gadget.id + "' name='" + gadget.id + "'>");

                        //set height
                        $gadget.height(slot.height);

                        //get token
                        var rpctoken = Math.round(Math.random() * 10000000);
                        while (AJS.WALLBOARD.rpcTokens[rpctoken]) {
                            rpctoken = Math.round(Math.random() * 10000000);
                        }
                        AJS.WALLBOARD.rpcTokens[rpctoken] = gadget.id;

                        //append token to url.
                        if (gadget.url.indexOf("#rpctoken") == -1) {
                            gadget.url = gadget.url + "#rpctoken=" + rpctoken;
                        } else {
                            gadget.url = gadget.url.replace(/#rpctoken=\d*/, "#rpctoken=" + rpctoken);
                        }
                        shindigGadgets.rpc.setAuthToken(gadget.id, rpctoken);

                        //set the url (with rpctoken appended) and write to the dom
                        $gadget.attr("src", gadget.url);
                        return $gadget;
                    } else {
                        var div = $("<div class='wallboard-gadget' id = '" + gadget.id + "' name = '" + gadget.id + "' height='300px'/>");
                        if (gadget.html) {
                            div.append($(_.unescape(gadget.html)));
                        }
                        return div;
                    }
                };

                var $gadget = gadgetHtml();

                $slot.append($gadget);

                if (gadget.amdModule) {
                    var gadgetObject = _.clone(gadget.jsonRepresentation);
                    gadgetObject.$ = $gadget;
                    gadgetObject.moduleAPI = new AG.InlineGadgetAPI(gadgetObject);
                    gadgetObject.isEditable = function () { return false; };

                    AG.Util.safeRender(function () {

                        var Module = require(gadget.jsonRepresentation.amdModule);
                        new Module(gadgetObject.moduleAPI).render(gadgetObject.$, AG.Util.extractUserPreferences(gadgetObject.userPrefs.fields));

                    }, gadgetObject.$, gadgetObject);
                }



                //set an onload to hide the blanket and add the css conversion if possible
                var onLoad = function () {
                    //increment iframesLoaded
                    AJS.WALLBOARD.iframesLoaded++;
                    //if we've hit the numIframes, hide the blanket
                    if (AJS.WALLBOARD.iframesLoaded == AJS.WALLBOARD.numIframes) {
                        $("#blanket").hide();
                    }
                    //if it's non-wallboardable, set an onload to write the style sheet
                    if (!gadget.isWallboardable) {
                        $(this.contentDocument).find("head").append('<link href="' + AJS.WALLBOARD.convertCssUrl + '" media="all" rel="stylesheet" type="text/css">');
                    }
                };

                if (gadget.url) {
                    $gadget.one('load', onLoad);
                } else {
                    // dashboard items are loaded instantly, they are not in iFrames
                    onLoad();
                }
            });

            if ($slot.find(".wallboard-gadget").size() >= 2) {
                $slot.cycle({
                    fx: 'fade',
                    timeout: 10000
                });
            }
        });
    };

    gadgets.IfrGadgetService.prototype.setTitle = function(title) {

    };
    function adjustHeights(slotId, height, $parent) {
        AJS.WALLBOARD.slotHeights[slotId] = height;
        $parent.height(height);

        //Height has increased so we need to destroy the old bindings and reinitialize cycling.
        if ($parent.find(".wallboard-gadget").size() >= 2) {
            $parent.cycle('destroy');
            $parent.children().each(function() {
                $(this).height(height);
            });
            $parent.cycle({
                fx: 'fade',
                timeout: 10000
            });
        } else {
            $($parent.children()[0]).height(height);
        }

    }

    gadgets.IfrGadgetService.prototype.setHeight = function(height) {
        var $iframe = null;
        if (this.f && this.f != "") {
            $iframe = $("#" + this.f);
        } else if (this.t && this.t != "" && AJS.WALLBOARD.rpcTokens[this.t]) {
            $iframe = $("#" + AJS.WALLBOARD.rpcTokens[this.t]);
        }
        if ($iframe != null) {
            var slotId = $iframe.parent().attr("id");
            var $parent = $iframe.parent();
            if (AJS.WALLBOARD.slotHeights[slotId] && AJS.WALLBOARD.slotHeights[slotId] < height) {
                adjustHeights(slotId, height, $parent);
            }
        }
    };

    var shindigGadgets = gadgets;

    AJS.WALLBOARD.rpcTokens = {};

    AJS.WALLBOARD.slotHeights = {};

    AJS.WALLBOARD.securityTokens = {};


    AJS.WALLBOARD.updateSecurityTokens = function(params, successCallback) {
        $.ajax({
            type: "POST",
            url: AJS.WALLBOARD.baseURL + "/plugins/servlet/gadgets/security-tokens",
            data: params,
            dataType: "json",
            success: successCallback,
            error: function(request, textStatus, errorThrown) {
                if (request.status != 200) {
                    AJS.log(
                            "Failed to get new security tokens. Response was had a status of '" +
                                    request.status + "' saying '" + request.statusText + "'");
                }
                else {
                    AJS.log("There was an error processing the response. Error was '" +
                            textStatus + "'");
                }
            }
        });
    };

    AJS.WALLBOARD.init = function(baseURL) {
        AJS.WALLBOARD.baseURL = baseURL;

        var blanketTimer = 0, securityTokenRefreshRate = AJS.parseUri(document.location.href).queryKey["__st_refresh"] || 1000 * 60 * 12;
        $("#build_monitor").height($(window).height());

        console.debug = console.debug || function() {
        };

        var $iframes = $(".wallboard-gadget");
        AJS.WALLBOARD.numIframes = $iframes.size();
        //if we've got no iframes, or if we've hit numIframes, hide the blanket
        if (AJS.WALLBOARD.numIframes == 0 || AJS.WALLBOARD.numIframes == AJS.WALLBOARD.iframesLoaded) {
            $("#blanket").hide();
        }

        //init  the security token for all gadgets
        $.each($iframes, function() {
            var $this = $(this), iframeUri = $this.attr("src");
            this.securityToken = decodeURIComponent(AJS.parseUri($this.attr("src")).queryKey["st"]);
            AJS.WALLBOARD.securityTokens[this.id] = this.securityToken;
        });

        function scheduledUpdateSecurityTokens() {
            var gadgetTokenFrames = new Array(), updateTokenParams = {};
            AJS.log("Updating all gadget security tokens");

            $.each($iframes, function(index) {
                var $this = $(this);
                gadgetTokenFrames.push({
                    gadget: $this,
                    iframeId: $this.attr("id")
                });
                updateTokenParams["st." + index] = this.securityToken;
            });
            if (!updateTokenParams["st.0"]) {
                AJS.log("No gadgets on dashboard, so there is no need to update security tokens.")
                return;
            }

            AJS.WALLBOARD.updateSecurityTokens(updateTokenParams, function(newSecurityTokens) {
                $.each(gadgetTokenFrames, function(index) {
                    this.securityToken = newSecurityTokens["st." + index];

                    //Store an escaped token in AJS.WALLBOARD.securityTokens, as it's the one which is used for hard refreshes
                    AJS.WALLBOARD.securityTokens[this.gadget.attr("id")] = encodeURIComponent(this.securityToken);
                    try {
                        shindigGadgets.rpc.call(this.iframeId, "update_security_token", null, this.securityToken);
                    }
                    catch (e) {
                        AJS.log(
                                "Unable to update the security token for gadget with iframe id " +
                                        this.iframeId + ".  This likely means that the gadget does not use the " +
                                        "'auth-refresh' feature.  If the gadget uses gadgets.io.makeRequest after its" +
                                        "initial startup, it is a good idea to use the 'auth-refresh' feature " +
                                        "by adding <Optional feature='auth-refresh' /> to your gadget's " +
                                        "<ModulePrefs> section.  Otherwise, the gadget's security token could expire" +
                                        " and subsequent calls to gadgets.io.makeRequest will fail.");
                    }
                });
                AJS.log("Updating security tokens complete.");
            });
        }

        AJS.log("Security tokens will be refreshed every " + securityTokenRefreshRate + "ms");
        window.setInterval(scheduledUpdateSecurityTokens, securityTokenRefreshRate);

        $(window).keydown(function(event) {
            var dashboardId = undefined;
            //27 is the ascii character for the 'esc' key.
            if (event.keyCode == 27) {
                var params = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                _.each(params, function(param) {
                    param = param.split('=');
                    if (param[0] == "dashboardId") {
                        dashboardId = param[1];
                    }
                });
            }
            if (dashboardId != undefined) {
                window.location = baseURL + "/secure/Dashboard.jspa?selectPageId=" + dashboardId;
            }
        });

        gadgets.rpc.register("is_interaction_supported", function() {
            return false;
        });

    }
    return AJS;

});
